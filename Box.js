
function Box(x, y, w, h){
	this.x = x;
	this.y = y;
	this.w = w;
	this.h = h;
}

Box.prototype.right = function() {return this.x + this.w;};
Box.prototype.bottom = function() {return this.y + this.h;};
Box.prototype.left = function() {return this.x;};
Box.prototype.top = function() {return this.y;};

Box.prototype.isCollission = function (box){
	return ((this.right() > box.left()) &&
            (box.right() > this.left()) &&
            (this.bottom() > box.top()) &&
            (box.bottom() > this.top()));
}

Box.prototype.toString = function (){
	return "x:" + this.x + ", y:" + this.y + ", w:" + this.w + ", h:" + this.h;
}
