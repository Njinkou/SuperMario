'use strict';



var images;
var mario;
var bg;
var obstacles = [];
var GROUNDLEVEL = 360;

/**@description in dieser Funktion werden alle bilder geladen. Bilder werden hier geladen, 
 * damit sie auf vor den anderen Funktionen ausgeführt werden
 */
function preload() {
	/**
    @type {image}
*/
	var imgBackground = loadImage("assets/background.jpg");
	var imgMario = loadImage("assets/mario.jpg")
	var imgObstacle = loadImage("assets/obstacle.jpg")
    images = {
		background: imgBackground,
		mario: imgMario,
		obstacle: imgObstacle
	};
    console.log("end of preload");
}

/**@description Hier wird Canvas Objekten initialisiert und Klassen Mario, Backgroundund Obstacle instantiert
 * @typedef {object} Mario
 * @typedef {object} Background
 * @typedef {object} obstacles
 */
function setup() {
    createCanvas(800, 500);
    mario = new Mario();
	bg = new Background();
	for(var i=0; i<5; i++)
		obstacles.push(new Obstacle());
    console.log("end of setup");
}

function draw() {
	bg.draw();
	for(var i=0; i<obstacles.length; i++){
		obstacles[i].draw();
		if (obstacles[i].box.isCollission(mario.box)) {
			console.log("collission: " + mario.box.toString() + ", " + obstacles[i].box.toString());
			noLoop();
		}
	}
    mario.draw();
}
/**@description Die Funktion definiert was passiert wenn einen beliebigen knopf auf dem Tastatur gedrueckt wird
 */
function keyPressed() {
  mario.jump();
}

/**@description Definier das Ereignis beim Mausklick
 */
function mousePressed() {
  loop();
}